package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/slaza/cms/server/utils/response"

	"github.com/google/uuid"

	"gitlab.com/slaza/cms/server/db/wrapper/gallery"
	"gitlab.com/slaza/cms/server/errors"

	"gitlab.com/slaza/cms/server/entities"
)

func (s *service) HandleGallerySummary() http.HandlerFunc {
	type outputType struct {
		Gallery []entities.GalleryImage `json:"images"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var output outputType

		result, err := gallery.Summary(s.GetContext())
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		output.Gallery = result
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			err := errors.NewFromErr(stdErr)
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleGalleryAdd() http.HandlerFunc {
	type inputType struct {
		Description string `json:"description"`
		Path        string `json:"path"`
	}

	type outputType struct {
		ID uuid.UUID `json:"id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		ctx := s.GetContext()
		id, err := gallery.Add(ctx, input.Description, input.Path)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		output.ID = id
		resp, _ := json.Marshal(output)
		response.Write(w, resp)
	}
}

func (s *service) HandleGalleryUpdate() http.HandlerFunc {
	type inputType struct {
		Image entities.GalleryImage `json:"image"`
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		ctx := s.GetContext()
		if err := gallery.Update(ctx, input.Image); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		resp, _ := json.Marshal(output)
		response.Write(w, resp)
	}
}

func (s *service) HandleGalleryDelete() http.HandlerFunc {
	type inputType struct {
		ID uuid.UUID `json:"id"`
	}

	type outputType struct {
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}
		ctx := s.GetContext()
		if err := gallery.Delete(ctx, input.ID); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		resp, _ := json.Marshal(output)
		response.Write(w, resp)
	}
}
