package errors

import (
	"fmt"
	"net/http"
	"runtime/debug"
)

const (
	InternalMsg = "Internal"
)

type Error struct {
	err   error
	code  int
	stack string
}

func (e Error) Error() string {
	return fmt.Sprintf("Error: %v\nStackTrace:\n%v\n", e.err.Error(), e.stack)
}

var (
	NoErr = Error{nil, http.StatusOK, ""}
)

func NewErr() Error {
	return Error{
		err:   fmt.Errorf("%v", InternalMsg),
		code:  http.StatusInternalServerError,
		stack: string(debug.Stack()),
	}
}

func NewFromErr(err error) Error {
	if err == nil {
		return NewErr()
	}
	return Error{
		err:   err,
		code:  http.StatusInternalServerError,
		stack: string(debug.Stack()),
	}
}

func (err Error) GetHTTPErrorResponse() (string, int) {
	return http.StatusText(err.code), err.code
}
