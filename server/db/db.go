package db

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/slaza/cms/server/config"
	"gitlab.com/slaza/cms/server/defaults"
)

func GetConnection() (*sql.DB, error) {
	connectionParams := fmt.Sprintf(
		"host=%s port=%d dbname=%s user=%s password='%s' sslmode=disable search_path=%s",
		defaults.Host,
		defaults.Port,
		defaults.DbName,
		defaults.User,
		defaults.Password,
		defaults.SearchPath,
	)
	return sql.Open("postgres", connectionParams)
}

func GetDB(ctx context.Context) *sql.DB {
	db, ok := ctx.Value(config.DBKey).(*sql.DB)
	if !ok {
		panic("DB not found in context!")
	}
	return db
}
