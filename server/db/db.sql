CREATE TABLE public.timeline
(
    id uuid,
    order_number integer DEFAULT 0,
    title text,
    PRIMARY KEY
    (id)
);

CREATE TABLE public.timeline_event
(
    id uuid NOT NULL,
    timeline_id uuid,
    date date,
    description text COLLATE pg_catalog
    ."default",
    CONSTRAINT timeline_event_pkey PRIMARY KEY
    (id)
);

ALTER TABLE public.timeline_event ADD CONSTRAINT FK_timeline_event__timeline_id 
    FOREIGN KEY(timeline_id)
    REFERENCES timeline(id);

CREATE TABLE public."article"
(
    id uuid NOT NULL,
    "time" timestamp
    without time zone NOT NULL,
    title character varying COLLATE pg_catalog."default" NOT NULL,
    content text COLLATE pg_catalog."default" NOT NULL,
    cover_img_path text COLLATE pg_catalog."default",
    img_path text COLLATE pg_catalog."default",
    CONSTRAINT "Article_pkey" PRIMARY KEY(id)
);


CREATE TABLE public.category
(
    id uuid NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT category_pkey PRIMARY KEY
    (id)
);


CREATE TABLE public.article_category
(
    id uuid NOT NULL,
    article_id uuid NOT NULL,
    category_id uuid NOT NULL,
    PRIMARY KEY (id)
);


ALTER TABLE public.article_category ADD CONSTRAINT FK_article_category__article_id 
    FOREIGN KEY(article_id)
    REFERENCES article(id);


ALTER TABLE public.article_category ADD CONSTRAINT FK_article_category__category_id 
    FOREIGN KEY(category_id)
    REFERENCES category(id);

CREATE TABLE public.gallery_images
(
    id uuid,
    path text NOT NULL,
    description text NOT NULL,
    PRIMARY KEY (id)
)