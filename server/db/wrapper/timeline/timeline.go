package timelinewrapper

import (
	"context"

	"sort"

	"github.com/google/uuid"
	dbWrapper "gitlab.com/slaza/cms/server/db"
	"gitlab.com/slaza/cms/server/entities"

	"gitlab.com/slaza/cms/server/errors"
)

func AddTimeline(ctx context.Context, title string, orderNumber int) (uuid.UUID, errors.Error) {
	db := dbWrapper.GetDB(ctx)

	id := uuid.New()
	_, err := db.Exec(`
		INSERT INTO public.timeline
		("id", "title", "order_number")
			VALUES ($1, $2, $3);`,
		id,
		title,
		orderNumber,
	)
	if err != nil {
		return uuid.UUID{}, errors.NewFromErr(err)
	}
	return id, errors.NoErr
}

func deleteTimelineEvents(ctx context.Context, t entities.TimelineType) errors.Error {
	db := dbWrapper.GetDB(ctx)

	_, stdErr := db.Exec(`
		DELETE FROM public.timeline_event
		WHERE "timeline_id" = $1;`,
		t.ID,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}

	return errors.NoErr
}

func DeleteTimeline(ctx context.Context, t entities.TimelineType) errors.Error {
	if err := deleteTimelineEvents(ctx, t); err != errors.NoErr {
		return err
	}

	db := dbWrapper.GetDB(ctx)

	res, stdErr := db.Exec(`
		DELETE FROM public.timeline
		WHERE "id" = $1;`,
		t.ID,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rowsCount, stdErr := res.RowsAffected()
	if stdErr != nil || rowsCount != 1 {
		return errors.NewFromErr(stdErr)
	}
	return errors.NoErr
}

func UpdateTimeline(ctx context.Context, t entities.TimelineType) errors.Error {
	db := dbWrapper.GetDB(ctx)

	res, stdErr := db.Exec(`
		UPDATE public.timeline
		SET 
			"title"=$2,
			"order_number" = $3
		WHERE "id" = $1;`,
		t.ID,
		t.Title,
		t.OrderNumber,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rowsCount, stdErr := res.RowsAffected()
	if stdErr != nil || rowsCount != 1 {
		return errors.NewFromErr(stdErr)
	}
	return errors.NoErr
}

func Summary(ctx context.Context) (entities.TimelineSummary, errors.Error) {
	var result entities.TimelineSummary

	db := dbWrapper.GetDB(ctx)

	rows, stdErr := db.Query(`
	SELECT
		t.id as timeline_id,
		t.title,
		t.order_number,
		te.id as event_id,
		te.timeline_id as event_timeline_id,
		te.date,
		te.description
	FROM public.timeline t
	LEFT OUTER JOIN public.timeline_event te ON t.id = te.timeline_id;`,
	)
	if stdErr != nil {
		return result, errors.NewFromErr(stdErr)
	}
	defer rows.Close()

	var t entities.TimelineType
	var dbe entities.DBEventType
	resMap := make(map[entities.TimelineType][]entities.EventType)
	for rows.Next() {
		stdErr = rows.Scan(
			&t.ID,
			&t.Title,
			&t.OrderNumber,
			&dbe.ID,
			&dbe.TimelineID,
			&dbe.Date,
			&dbe.Description,
		)
		if stdErr != nil {
			return result, errors.NewFromErr(stdErr)
		}
		if !dbe.IsNil() {
			resMap[t] = append(resMap[t], dbe.ConvertToEvent())
		} else {
			resMap[t] = resMap[t]
		}
	}

	for key, val := range resMap {
		var item entities.TimelineWithEvents
		item.Timeline = key
		item.Events = val
		result.Timelines = append(result.Timelines, item)
	}
	sort.Sort(&result)

	return result, errors.NoErr
}
