package timelinewrapper

import (
	"context"

	dbWrapper "gitlab.com/slaza/cms/server/db"

	"github.com/google/uuid"

	"gitlab.com/slaza/cms/server/entities"
	"gitlab.com/slaza/cms/server/errors"
)

func AddEvent(ctx context.Context, event entities.EventType) (uuid.UUID, errors.Error) {
	db := dbWrapper.GetDB(ctx)

	id := uuid.New()
	_, err := db.Exec(`
		INSERT INTO public.timeline_event
		("id", "timeline_id", "date", "description")
			VALUES ($1, $2, $3, $4);`,
		id,
		event.TimelineID,
		event.Date.Value,
		event.Description,
	)
	if err != nil {
		return uuid.UUID{}, errors.NewFromErr(err)
	}
	return id, errors.NoErr
}

func UpdateEvent(ctx context.Context, event entities.EventType) errors.Error {
	db := dbWrapper.GetDB(ctx)

	res, stdErr := db.Exec(`
		UPDATE public.timeline_event
		SET
			timeline_id = $2,
			date = $3,
			description = $4
		WHERE "id" = $1;`,
		event.ID,
		event.TimelineID,
		event.Date.Value,
		event.Description,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rows, stdErr := res.RowsAffected()
	if stdErr != nil || rows != 1 {
		return errors.NewFromErr(stdErr)
	}
	return errors.NoErr
}

func DeleteEvent(ctx context.Context, id uuid.UUID) errors.Error {
	db := dbWrapper.GetDB(ctx)

	res, err := db.Exec(`
		DELETE FROM public.timeline_event
		WHERE "id" = $1;`,
		id,
	)
	if err != nil {
		return errors.NewFromErr(err)
	}
	rows, stdErr := res.RowsAffected()
	if stdErr != nil || rows != 1 {
		return errors.NewFromErr(stdErr)
	}
	return errors.NoErr
}
