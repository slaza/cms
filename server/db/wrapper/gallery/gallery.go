package gallery

import (
	"context"

	"github.com/google/uuid"

	dbWrapper "gitlab.com/slaza/cms/server/db"
	"gitlab.com/slaza/cms/server/entities"
	"gitlab.com/slaza/cms/server/errors"
)

func Summary(ctx context.Context) ([]entities.GalleryImage, errors.Error) {
	result := make([]entities.GalleryImage, 0)

	db := dbWrapper.GetDB(ctx)
	rows, stdErr := db.QueryContext(ctx, `
		SELECT id, path, description 
		FROM gallery_images;`,
	)
	if stdErr != nil {
		return result, errors.NewFromErr(stdErr)
	}
	defer rows.Close()

	for rows.Next() {
		var img entities.GalleryImage
		stdErr := rows.Scan(
			&img.ID,
			&img.Path,
			&img.Description,
		)
		if stdErr != nil {
			return result, errors.NewFromErr(stdErr)
		}
		result = append(result, img)
	}
	return result, errors.NoErr
}

func Add(ctx context.Context, description, path string) (uuid.UUID, errors.Error) {
	id := uuid.New()
	db := dbWrapper.GetDB(ctx)

	response, stdErr := db.ExecContext(ctx, `
		INSERT INTO gallery_images
		(id, path, description)
		VALUES($1, $2, $3);`,
		id,
		path,
		description,
	)
	if stdErr != nil {
		return id, errors.NewFromErr(stdErr)
	}
	rowCount, stdErr := response.RowsAffected()
	if stdErr != nil || rowCount != 1 {
		return id, errors.NewFromErr(stdErr)
	}

	return id, errors.NoErr
}

func Delete(ctx context.Context, id uuid.UUID) errors.Error {
	db := dbWrapper.GetDB(ctx)

	response, stdErr := db.ExecContext(ctx, `
		DELETE FROM gallery_images
		WHERE id=$1;
	`,
		id,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}

	rowsAffected, stdErr := response.RowsAffected()
	if stdErr != nil || rowsAffected != 1 {
		return errors.NewFromErr(stdErr)
	}

	return errors.NoErr
}

func Update(ctx context.Context, image entities.GalleryImage) errors.Error {
	db := dbWrapper.GetDB(ctx)

	response, stdErr := db.ExecContext(ctx, `
		UPDATE gallery_images
		SET 
			description=$2,
			path=$3
		WHERE
			id=$1;
	`,
		image.ID,
		image.Description,
		image.Path,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rowsAffected, stdErr := response.RowsAffected()
	if stdErr != errors.NoErr || rowsAffected != 1 {
		return errors.NoErr
	}
	return errors.NoErr
}
