package article

import (
	"context"

	"github.com/google/uuid"
	dbWrapper "gitlab.com/slaza/cms/server/db"
	"gitlab.com/slaza/cms/server/entities"
	"gitlab.com/slaza/cms/server/errors"
)

func Add(ctx context.Context, article entities.Article) (uuid.UUID, errors.Error) {
	db := dbWrapper.GetDB(ctx)

	id := uuid.New()
	_, stdErr := db.ExecContext(ctx, `
		INSERT INTO public.article
		(id, "time", title, content, cover_img_path, img_path)
		VALUES($1, $2, $3, $4, $5, $6);
	`,
		id,
		article.Time.Value,
		article.Title,
		article.Content,
		article.CoverImgPath,
		article.ImgPath,
	)
	if stdErr != nil {
		return uuid.UUID{}, errors.NewFromErr(stdErr)
	}
	return id, errors.NoErr
}

func Update(ctx context.Context, article entities.Article) errors.Error {
	db := dbWrapper.GetDB(ctx)

	res, stdErr := db.ExecContext(ctx, `
		UPDATE public.article
		SET
			"time"=$2,
			title=$3,
			content=$4,
			cover_img_path=$5,
			img_path=$6
		WHERE
			id=$1;
	`,
		article.ID,
		article.Time.Value,
		article.Title,
		article.Content,
		article.CoverImgPath,
		article.ImgPath,
	)

	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rows, stdErr := res.RowsAffected()
	if stdErr != nil || rows != 1 {
		return errors.NewFromErr(stdErr)
	}

	return errors.NoErr
}

func Delete(ctx context.Context, article entities.Article) errors.Error {
	db := dbWrapper.GetDB(ctx)

	res, stdErr := db.ExecContext(ctx, `
	DELETE FROM public.article
		WHERE id = $1;`,
		article.ID,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}
	rows, stdErr := res.RowsAffected()
	if stdErr != nil || rows != 1 {
		return errors.NewFromErr(stdErr)
	}

	return errors.NoErr
}

func Search(ctx context.Context, title string) ([]entities.Article, errors.Error) {
	var result []entities.Article

	db := dbWrapper.GetDB(ctx)

	rows, stdErr := db.QueryContext(ctx, `
	SELECT
		id, "time", title, content, cover_img_path, img_path
	FROM public.article
		WHERE title ILIKE $1;`,
		"%"+title+"%",
	)
	if stdErr != nil {
		return result, errors.NewFromErr(stdErr)
	}
	defer rows.Close()

	for rows.Next() {
		a := entities.Article{}
		stdErr = rows.Scan(
			&a.ID,
			&a.Time.Value,
			&a.Title,
			&a.Content,
			&a.CoverImgPath,
			&a.ImgPath,
		)
		if stdErr != nil {
			return result, errors.NewFromErr(stdErr)
		}
		result = append(result, a)
	}

	return result, errors.NoErr
}

func appendCategory(ctx context.Context, articleID, categoryID uuid.UUID) (uuid.UUID, errors.Error) {
	db := dbWrapper.GetDB(ctx)

	id := uuid.New()
	_, stdErr := db.ExecContext(ctx, `
		INSERT INTO public.article_category
			(id, article_id, category_id)
			VALUES($1, $2, $3);
	`,
		id,
		articleID,
		categoryID,
	)
	if stdErr != nil {
		return uuid.UUID{}, errors.NewFromErr(stdErr)
	}

	return id, errors.NoErr
}

func AppendCategories(ctx context.Context, articleID uuid.UUID, list []entities.Category) ([]uuid.UUID, errors.Error) {
	var result []uuid.UUID

	for _, val := range list {
		id, err := appendCategory(ctx, articleID, val.ID)
		if err != errors.NoErr {
			return result, err
		}
		result = append(result, id)
	}
	return result, errors.NoErr
}

func GetCategories(ctx context.Context) ([]entities.Category, errors.Error) {
	var result []entities.Category

	db := dbWrapper.GetDB(ctx)

	rows, stdErr := db.QueryContext(ctx, `
		SELECT id, name
		FROM category;
		`,
	)
	if stdErr != nil {
		return result, errors.NewFromErr(stdErr)
	}

	defer rows.Close()

	for rows.Next() {
		var c entities.Category
		stdErr = rows.Scan(
			&c.ID,
			&c.Name,
		)
		if stdErr != nil {
			return result, errors.NewFromErr(stdErr)
		}
		result = append(result, c)
	}
	return result, errors.NoErr
}

func GetArticleCategories(ctx context.Context, articleID uuid.UUID) ([]entities.Category, errors.Error) {
	var result []entities.Category

	db := dbWrapper.GetDB(ctx)

	rows, stdErr := db.QueryContext(ctx, `
		SELECT c.id, c.name 
			FROM category c
			WHERE id IN
			(
				SELECT category_id
				FROM article_category ac
				WHERE article_id = $1	
			);
	`,
		articleID,
	)
	if stdErr != nil {
		return result, errors.NewFromErr(stdErr)
	}

	defer rows.Close()

	for rows.Next() {
		var c entities.Category
		stdErr = rows.Scan(
			&c.ID,
			&c.Name,
		)
		if stdErr != nil {
			return result, errors.NewFromErr(stdErr)
		}
		result = append(result, c)
	}
	return result, errors.NoErr
}

func DeleteCategories(ctx context.Context, articleID uuid.UUID) errors.Error {
	db := dbWrapper.GetDB(ctx)
	_, stdErr := db.ExecContext(
		ctx, `
		DELETE FROM article_category
		WHERE article_id = $1;
	`,
		articleID,
	)
	if stdErr != nil {
		return errors.NewFromErr(stdErr)
	}

	return errors.NoErr
}

func UpdateCategories(ctx context.Context, articleID uuid.UUID, categories []entities.Category) errors.Error {
	if err := DeleteCategories(ctx, articleID); err != errors.NoErr {
		return err
	}

	_, err := AppendCategories(ctx, articleID, categories)

	return err
}
