package config

type ctxKeyType int

//Time format: Mon Jan 2 15:04:05 -0700 MST 2006
const (
	DBKey ctxKeyType = iota
	ResponseKey
	RequestKey

	DateFormat     = "2006-01-02"
	DateTimeFormat = "2006-01-02 15:04:05" //example of ISO 8601: 2013-02-08 09:30:26
	Port           = ":8080"
	FsPort         = ":8081"
)
