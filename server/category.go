package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/slaza/cms/server/db/wrapper/article"
	"gitlab.com/slaza/cms/server/errors"
	"gitlab.com/slaza/cms/server/utils/response"

	"gitlab.com/slaza/cms/server/entities"
)

func (s *service) HandleCategoryGetAll() http.HandlerFunc {
	type inputType struct {
	}

	type outputType struct {
		List []entities.Category `json:"categories"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var output outputType

		ctx := s.GetContext()
		categories, err := article.GetCategories(ctx)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		output.List = categories
		res, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, res)
	}
}
