package main

import (
	"context"
	"database/sql"
	"log"
	"net/http"

	"gitlab.com/slaza/cms/server/defaults"

	"github.com/gorilla/mux"

	_ "github.com/lib/pq"
	"gitlab.com/slaza/cms/server/config"
	"gitlab.com/slaza/cms/server/db"
)

type service struct {
	Db     *sql.DB
	Router *mux.Router
}

func (s *service) GetContext() context.Context {
	return context.WithValue(context.Background(), config.DBKey, s.Db)
}

func main() {
	db, err := db.GetConnection()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)

	s := &service{
		Db:     db,
		Router: mux.NewRouter(),
	}
	s.Routes()

	log.Printf("Server started on port %v...\n", config.Port)

	go fileServer()
	log.Fatal(http.ListenAndServe(config.Port, s))
}

func (s *service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Router.ServeHTTP(w, r)
}

func fileServer() {
	fsPort := config.FsPort
	directory := defaults.FileUploadPath
	log.Printf("File server serving at HTTP port %v, for directory %v", fsPort, directory)

	http.Handle("/", http.FileServer(http.Dir(directory)))

	log.Fatal(http.ListenAndServe(fsPort, nil))
}
