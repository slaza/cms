package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/slaza/cms/server/errors"
	"gitlab.com/slaza/cms/server/utils/response"

	"gitlab.com/slaza/cms/server/db/wrapper/article"

	"github.com/google/uuid"

	"gitlab.com/slaza/cms/server/entities"
)

func (s *service) HandleArticleAdd() http.HandlerFunc {
	type inputType struct {
		Article    entities.Article    `json:"article"`
		Categories []entities.Category `json:"categories"`
	}

	type outputType struct {
		ID uuid.UUID `json:"id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		ctx := s.GetContext()
		id, err := article.Add(ctx, input.Article)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		_, err = article.AppendCategories(ctx, id, input.Categories)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		output.ID = id
		res, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
		}
		response.Write(w, res)
	}
}

func (s *service) HandleArticleUpdate() http.HandlerFunc {
	type inputType struct {
		Article    entities.Article    `json:"article"`
		Categories []entities.Category `json:"categories"`
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		err := article.Update(s.GetContext(), input.Article)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
		}

		_, err = article.AppendCategories(s.GetContext(), input.Article.ID, input.Categories)
		if err != errors.NoErr {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(err.Error())
		}

		res, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
		}
		response.Write(w, res)
	}
}

func (s *service) HandleArticleDelete() http.HandlerFunc {
	type inputType struct {
		entities.Article
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		ctx := s.GetContext()

		if err := article.DeleteCategories(ctx, input.Article.ID); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		if err := article.Delete(ctx, input.Article); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}

		res, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, res)
	}
}

func (s *service) HandleArticleSearch() http.HandlerFunc {
	type inputType struct {
		Title string `json:"title"`
	}

	type outputType struct {
		SerchResult []entities.Article `json:"search_result"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		articles, err := article.Search(s.GetContext(), input.Title)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		output.SerchResult = articles

		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleGetArticleCategories() http.HandlerFunc {
	type inputType struct {
		ID uuid.UUID `json:"article_id"`
	}

	type outputType struct {
		Categories []entities.Category `json:"categories"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		ctx := s.GetContext()
		categories, err := article.GetArticleCategories(ctx, input.ID)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		output.Categories = categories

		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}
