package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/slaza/cms/server/utils/response"

	"github.com/google/uuid"
	"gitlab.com/slaza/cms/server/entities"
	"gitlab.com/slaza/cms/server/errors"

	timelinewrapper "gitlab.com/slaza/cms/server/db/wrapper/timeline"
)

func (s *service) HandleEventAdd() http.HandlerFunc {

	type inputType struct {
		Event entities.EventType `json:"event"`
	}

	type outputType struct {
		ID uuid.UUID `json:"id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		id, err := timelinewrapper.AddEvent(s.GetContext(), input.Event)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}
		output.ID = id
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleEventDelete() http.HandlerFunc {
	type inputType struct {
		ID uuid.UUID `json:"Id"`
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		if err := timelinewrapper.DeleteEvent(s.GetContext(), input.ID); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleEventUpdate() http.HandlerFunc {
	type inputType struct {
		Event entities.EventType `json:"event"`
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		if err := timelinewrapper.UpdateEvent(s.GetContext(), input.Event); err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleTimelineAdd() http.HandlerFunc {
	type inputType struct {
		Title       string `json:"title"`
		OrderNumber int    `json:"order_number"`
	}
	type outputType struct {
		ID uuid.UUID `json:"id"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		id, err := timelinewrapper.AddTimeline(s.GetContext(), input.Title, input.OrderNumber)
		if err != errors.NoErr {
			log.Println(err.Error())
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}
		output.ID = id

		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleTimelineUpdate() http.HandlerFunc {
	type inputType struct {
		Timeline entities.TimelineType `json:"timeline"`
	}

	type outputType struct {
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		err := timelinewrapper.UpdateTimeline(s.GetContext(), input.Timeline)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}

		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleTimelineDelete() http.HandlerFunc {
	type inputType struct {
		Timeline entities.TimelineType `json:"timeline"`
	}

	type outputType struct {
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var input inputType
		var output outputType

		if stdErr := json.NewDecoder(r.Body).Decode(&input); stdErr != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			log.Println(stdErr.Error())
			return
		}

		err := timelinewrapper.DeleteTimeline(s.GetContext(), input.Timeline)
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			return
		}

		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}

func (s *service) HandleTimelineSummary() http.HandlerFunc {
	type inputType struct {
	}

	type outputType struct {
		entities.TimelineSummary
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var output outputType

		res, err := timelinewrapper.Summary(s.GetContext())
		if err != errors.NoErr {
			msg, code := err.GetHTTPErrorResponse()
			http.Error(w, msg, code)
			log.Println(err.Error())
			return
		}
		output.TimelineSummary = res
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}
