package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"gitlab.com/slaza/cms/server/config"

	"gitlab.com/slaza/cms/server/utils/response"

	"gitlab.com/slaza/cms/server/defaults"
)

func (s *service) HandleUploadFile() http.HandlerFunc {
	type outputType struct {
		Path string `json:"file_path"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var output outputType

		r.ParseMultipartForm(10 << 20)
		file, handler, stdErr := r.FormFile("File")
		if stdErr != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Printf("Error reading file: %v\n", stdErr.Error())
			return
		}
		defer file.Close()

		log.Printf(
			`Uploaded file: [name: %+v, size: %+v, mime header: %+v]\n`,
			handler.Filename,
			handler.Size,
			handler.Header,
		)
		nameParts := strings.Split(handler.Filename, ".")
		if len(nameParts) != 2 {
			log.Printf("Extension not found for file %v\n", handler.Filename)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		extemsion := nameParts[1]

		tempFile, err := ioutil.TempFile(defaults.FileUploadPath, "upload-*."+extemsion)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		defer tempFile.Close()

		fileBytes, stdErr := ioutil.ReadAll(file)
		if stdErr != nil {
			code := http.StatusInternalServerError
			msg := http.StatusText(code)
			http.Error(w, msg, code)
			log.Println(stdErr.Error())
			return
		}
		tempFile.Write(fileBytes)

		nameComponents := strings.Split(tempFile.Name(), "/")
		output.Path = fmt.Sprintf("http://localhost%v/%v", config.FsPort, nameComponents[len(nameComponents)-1])
		resp, stdErr := json.Marshal(output)
		if stdErr != nil {
			code := http.StatusInternalServerError
			msg := http.StatusText(code)
			http.Error(w, msg, code)
			log.Println(stdErr.Error())
			return
		}
		response.Write(w, resp)
	}
}
