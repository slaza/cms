package entities

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/slaza/cms/server/config"
)

func Test_MarshalUnmarshalTimelineType(t *testing.T) {
	const input = `
	{
		"id": "9da2f32d-6f96-41ef-9190-802c5d36d88d",
		"title": "Test",
		"order_number" : 2
	}
	`
	var timeline TimelineType
	err := json.Unmarshal([]byte(input), &timeline)
	assert.NoError(t, err)
	assert.Equal(t, timeline.ID.String(), "9da2f32d-6f96-41ef-9190-802c5d36d88d")
	assert.Equal(t, timeline.Title, "Test")
	assert.Equal(t, timeline.OrderNumber, int32(2))

	res, err := json.Marshal(timeline)
	response := string(res)
	assert.NoError(t, err)
	assert.JSONEq(t, input, response)
}

func Test_MarshalUnmarshalEventType(t *testing.T) {
	const input = `
	{
		"id": "9da2f32d-6f96-41ef-9190-802c5d36d88d",
		"timeline_id":"9d666c00-ecc1-49c1-ad00-e3b2b7ba32f6",
		"date": "2010-03-05",
		"description": "Test description"
	}
	`

	const (
		expectedID          = "9da2f32d-6f96-41ef-9190-802c5d36d88d"
		expectedTimelineID  = "9d666c00-ecc1-49c1-ad00-e3b2b7ba32f6"
		expectedDescription = "Test description"
		expectedDateString  = "2010-03-05"
	)

	expectedDate, err := time.Parse(config.DateFormat, expectedDateString)
	assert.NoError(t, err)
	var event EventType
	err = json.Unmarshal([]byte(input), &event)
	assert.NoError(t, err)
	assert.Equal(t, expectedID, event.ID.String())
	assert.Equal(t, expectedTimelineID, event.TimelineID.String())
	assert.Equal(t, expectedDescription, event.Description)
	assert.Equal(t, expectedDate, event.Date.Value)

	res, err := json.Marshal(event)
	response := string(res)
	assert.NoError(t, err)
	assert.JSONEq(t, input, response)
}
