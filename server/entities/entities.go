package entities

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/slaza/cms/server/utils/optional"

	"gitlab.com/slaza/cms/server/config"

	"github.com/google/uuid"
)

type DateType struct {
	Value time.Time
}

type DateTimeType struct {
	Value time.Time
}

func (dt *DateTimeType) UnmarshalJSON(b []byte) (err error) {
	s := string(b)
	s = s[1 : len(s)-1]

	if dt.Value, err = time.Parse(config.DateTimeFormat, s); err != nil {
		return fmt.Errorf(`Error unmarshaling datetime :"%v\n%v"`, s, err.Error())
	}
	return nil
}

func (dt DateTimeType) MarshalJSON() ([]byte, error) {
	s := dt.Value.Format(config.DateTimeFormat)
	return json.Marshal(s)
}

func (t *DateType) UnmarshalJSON(b []byte) (err error) {
	s := string(b)
	s = s[1 : len(s)-1]

	if t.Value, err = time.Parse(config.DateFormat, s); err != nil {
		return fmt.Errorf(`Error unmarshaling time:"%v"\n%v`, s, err.Error())
	}
	return nil
}

func (t DateType) MarshalJSON() ([]byte, error) {
	s := t.Value.Format(config.DateFormat)
	return json.Marshal(s)
}

type EventType struct {
	ID          uuid.UUID `json:"id"`
	TimelineID  uuid.UUID `json:"timeline_id"`
	Date        DateType  `json:"date"`
	Description string    `json:"description"`
}

type DBEventType struct {
	ID          *uuid.UUID `json:"id"`
	TimelineID  *uuid.UUID `json:"timeline_id"`
	Date        *time.Time `json:"date"`
	Description *string    `json:"description"`
}

func (event DBEventType) IsNil() bool {
	if event.ID == nil || event.TimelineID == nil || event.Date == nil || event.Description == nil {
		return true
	}
	return false
}

func (event DBEventType) ConvertToEvent() EventType {
	return EventType{
		ID:          optional.UUID(event.ID),
		TimelineID:  optional.UUID(event.TimelineID),
		Date:        DateType{optional.Time(event.Date)},
		Description: optional.String(event.Description),
	}
}
func (e EventType) Strig() string {
	return fmt.Sprintf("{id: %v, timeline: %v, date: %v, description: %v}",
		e.ID,
		e.TimelineID,
		e.Date,
		e.Description,
	)
}

type TimelineType struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	OrderNumber int32     `json:"order_number"`
}

type TimelineSummary struct {
	Timelines []TimelineWithEvents
}

func (summary *TimelineSummary) Len() int {
	return len(summary.Timelines)
}

func (summary *TimelineSummary) Swap(i, j int) {
	a := summary.Timelines[i]
	b := summary.Timelines[j]
	summary.Timelines[i] = b
	summary.Timelines[j] = a
}

func (summary *TimelineSummary) Less(i, j int) bool {
	first := summary.Timelines[i].Timeline.OrderNumber
	second := summary.Timelines[j].Timeline.OrderNumber
	return first < second
}

type TimelineWithEvents struct {
	Timeline TimelineType `json:"timeline"`
	Events   []EventType  `json:"events"`
}

type Article struct {
	ID           uuid.UUID    `json:"id"`
	Time         DateTimeType `json:"time"`
	Title        string       `json:"title"`
	Content      string       `json:"content"`
	CoverImgPath string       `json:"cover"`
	ImgPath      string       `json:"image"`
}

type Category struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}

type GalleryImage struct {
	ID          uuid.UUID `json:"id"`
	Path        string    `json:"path"`
	Description string    `json:"description"`
}
