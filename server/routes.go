package main

func (s *service) Routes() {
	s.Router.HandleFunc("/timeline/event_add", s.HandleEventAdd())
	s.Router.HandleFunc("/timeline/event_update", s.HandleEventUpdate())
	s.Router.HandleFunc("/timeline/event_delete", s.HandleEventDelete())

	s.Router.HandleFunc("/timeline/timeline_add", s.HandleTimelineAdd())
	s.Router.HandleFunc("/timeline/timeline_update", s.HandleTimelineUpdate())
	s.Router.HandleFunc("/timeline/timeline_delete", s.HandleTimelineDelete())
	s.Router.HandleFunc("/timeline/timeline_summary", s.HandleTimelineSummary())

	s.Router.HandleFunc("/article/article_add", s.HandleArticleAdd())
	s.Router.HandleFunc("/article/article_update", s.HandleArticleUpdate())
	s.Router.HandleFunc("/article/article_delete", s.HandleArticleDelete())
	s.Router.HandleFunc("/article/search", s.HandleArticleSearch())

	s.Router.HandleFunc("/category/article_categories", s.HandleGetArticleCategories())
	s.Router.HandleFunc("/category/get_all", s.HandleCategoryGetAll())

	s.Router.HandleFunc("/gallery_summary", s.HandleGallerySummary())
	s.Router.HandleFunc("/gallery_add", s.HandleGalleryAdd())
	s.Router.HandleFunc("/gallery_update", s.HandleGalleryUpdate())
	s.Router.HandleFunc("/gallery_delete", s.HandleGalleryDelete())

	s.Router.HandleFunc("/upload", s.HandleUploadFile())
}
