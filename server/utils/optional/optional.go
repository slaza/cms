package optional

import (
	"time"

	"github.com/google/uuid"
)

func String(v *string) string {
	if v != nil {
		return *v
	}
	return ""
}

func Time(v *time.Time) time.Time {
	if v != nil {
		return *v
	}
	return time.Time{}
}

func UUID(v *uuid.UUID) uuid.UUID {
	if v != nil {
		return *v
	}
	return uuid.UUID{}
}
