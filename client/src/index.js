import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Route, BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import ArticleApp from "./pages/ArticleApp";
import ArticleEditApp from "./pages/ArticleEditApp";
import ArticlePreviewApp from "./pages/ArticlePreviewApp";
import ArticleAddApp from "./pages/ArticleAddApp";
import * as serviceWorker from "./serviceWorker";
import TimelineApp from "./pages/TimelineApp";
import GalleryApp from "./pages/GalleryApp";

function Routing() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={App} />
        <Route exact path="/article" component={ArticleApp} />
        <Route exact path="/timeline" component={TimelineApp} />
        <Route exact path="/edit_article/:id" component={ArticleEditApp} />
        <Route exact path="/add_article" component={ArticleAddApp} />
        <Route exact path="/preview_article" component={ArticlePreviewApp} />
        <Route exact path="/gallery" component={GalleryApp} />
      </div>
    </Router>
  );
}

ReactDOM.render(<Routing />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
