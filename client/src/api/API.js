const urlbase = "http://127.0.0.1:8080";

export async function getSearchPromise(articleName) {
  const url = `${urlbase}/article/search`;
  const param = { title: articleName };

  return fetch(url, {
    method: "POST",
    body: JSON.stringify(param)
  })
    .then(response => response.json())
    .then(response => response.search_result)
    .catch(err => {
      alert(`Error on upload: ${err}`);
      console.error(`error on upload ${err}`);
    });
}

export async function uploadImage(file) {
  const url = `${urlbase}/upload`;
  if (file != null) {
    const data = new FormData();
    data.append("File", file);
    return fetch(url, {
      method: "POST",
      body: data
    })
      .then(response => response.json())
      .then(response => response.file_path);
  }
}

export async function getArticleCategories(articleID) {
  const url = `${urlbase}/category/article_categories`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify({ article_id: articleID })
  })
    .then(response => response.json())
    .then(response => response.categories)
    .then(response => (response === null ? [] : response));
}

export async function getCategories() {
  const url = `${urlbase}/category/get_all`;
  return fetch(url, {
    method: "GET"
  })
    .then(response => response.json())
    .then(response => response.categories);
}

export async function editArticle(article, categories) {
  const url = `${urlbase}/article/article_update`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify({ article: article, categories: categories })
  });
}

export async function deleteArticle(article) {
  const url = `${urlbase}/article/article_delete`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify(article)
  });
}

export async function addArticle(article, categories) {
  const url = `${urlbase}/article/article_add`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify({ article: article, categories: categories })
  });
}

export async function addEvent(timelineId, date, description) {
  const url = `${urlbase}/timeline/event_add`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify({
      event: {
        timeline_id: timelineId,
        date: date,
        description: description
      }
    })
  })
    .then(response => response.json())
    .then(response => response.id);
}

export async function deleteEvent(eventId) {
  const url = `${urlbase}/timeline/event_delete`;
  return fetch(url, {
    method: "POST",
    body: JSON.stringify({
      id: eventId
    })
  });
}

export async function updateEvent(eventId, timelineId, description, date) {
  const url = `${urlbase}/timeline/event_update`;
  const body = JSON.stringify({
    event: {
      id: eventId,
      timeline_id: timelineId,
      description: description,
      date: date
    }
  });
  return fetch(url, {
    method: "POST",
    body: body
  });
}

export async function updateTimeline(timelineId, title, orderNumber) {
  const url = `${urlbase}/timeline/timeline_update`;
  const body = JSON.stringify({
    timeline: {
      id: timelineId,
      title: title,
      order_number: orderNumber
    }
  });
  return fetch(url, {
    method: "POST",
    body: body
  });
}

export async function addGalleryImage(description, path) {
  const url = `${urlbase}/gallery_add`;
  const body = JSON.stringify({
    description: description,
    path: path
  });
  return fetch(url, {
    method: "POST",
    body: body
  })
    .then(response => response.json())
    .then(response => response.id);
}

export async function updateGalleryImage(id, description, path) {
  const url = `${urlbase}/gallery_update`;
  const body = JSON.stringify({
    image: {
      id: id,
      description: description,
      path: path
    }
  });
  return fetch(url, {
    method: "POST",
    body: body
  });
}

export async function deleteGalleryImage(id) {
  const url = `${urlbase}/gallery_delete`;
  const body = JSON.stringify({
    id: id
  });
  return fetch(url, {
    method: "POST",
    body: body
  });
}

export async function gallerySummary() {
  return fetch("http://127.0.0.1:8080/gallery_summary", {
    method: "GET"
  })
    .then(response => response.json())
    .then(response => response.images);
}

export default getSearchPromise;
