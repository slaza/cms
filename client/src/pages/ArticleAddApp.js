import React, { useState, useEffect } from "react";
import { getCategories, addArticle } from "../api/API";
import DateTime from "react-datetime";
import moment from "moment";
import Navigation from "../components/Navigation";
import Upload from "../components/Upload";

function ArticleAddApp() {
  const dateTimeFormat = "YYYY-MM-DD hh:mm:ss";

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [categories, setCategories] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [dateTime, setDateTime] = useState(null);
  const [imageUrl, setImageUrl] = useState("");
  const [coverUrl, setCoverUrl] = useState("");

  useEffect(function() {
    getCategories()
      .then(categories => setCategories(categories))
      .catch(err => alert(`Error: ${err}`));
  }, []);

  function onContentChange(e) {
    setContent(e.target.value);
  }

  function onMultiselectChange(e) {
    let selected = [];
    for (let i = 0; i < e.target.length; i++) {
      const item = e.target[i];
      if (item.selected === true) {
        selected.push({ id: item.value });
      }
    }
    for (let i = 0; i < selected.length; i++) {
      for (let j = 0; j < categories.length; j++) {
        if (selected[i].id === categories[j].id) {
          selected[i].name = categories[j].name;
        }
      }
    }
    setSelectedCategories(selected);
  }

  function onDateChange(e) {
    setDateTime(e.format(dateTimeFormat));
  }

  function onCoverSubmit(url) {
    setCoverUrl(url);
  }

  function onImageSubmit(url) {
    setImageUrl(url);
  }

  function submitHandler(e) {
    e.preventDefault();
    if (!title || title === "") {
      alert("Title is empty!");
      return;
    }
    if (!content || content === "") {
      alert("Content is empty!");
      return;
    }
    const article = {
      time: dateTime,
      title: title,
      content: content,
      cover: coverUrl,
      image: imageUrl
    };
    addArticle(article, selectedCategories)
      .then(alert("Success"))
      .catch(`Error: ${e}`);
  }

  const labelStyle = {
    background: "#2e6ca6",
    color: "#ffffff",
    fontWeight: "bold",
    textAlign: "center",
    margin: "5px",
    width: "100%"
  };

  return (
    <div>
      <Navigation />
      <div>
        <h2 style={{ fontWeight: "bolder" }}>New Article</h2>
      </div>
      <div style={{ textAlign: "center" }}>
        <form>
          <div>
            <div>
              <h2 style={labelStyle}>Title:</h2>
            </div>
            <input
              type="text"
              onChange={e => setTitle(e.target.value)}
              style={{
                marginLeft: "10px",
                width: "50%",
                fontSize: "20px",
                textAlign: "center"
              }}
            />
          </div>
          <div>
            <h2 style={labelStyle}>Content</h2>
            <br />
            <textarea
              value={content}
              onChange={onContentChange}
              style={{
                resize: "none",
                width: "60%",
                height: "500px",
                fontSize: "14px"
              }}
            />
          </div>
          <div>
            <h2 style={labelStyle}>Categories</h2>
            <select multiple={true} onChange={onMultiselectChange}>
              {categories.map((category, index) => (
                <option value={category.id} key={category.id}>
                  {index + ") " + category.name}
                </option>
              ))}
            </select>
          </div>
          <div>
            <h2 style={labelStyle}>Time</h2>
            <DateTime
              id="dateTime"
              defaultValue={moment()}
              onChange={onDateChange}
              locale="en-US"
              timeFormat="HH:mm"
              dateFormat
              closeOnSelect
              input={false}
            />
          </div>
          {/*Article Cover*/}

          <div>
            <table
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                textAlign: "center"
              }}
            >
              <tbody>
                <tr>
                  <td>
                    <h3>Cover</h3>
                  </td>
                  <td>
                    <Upload onUpload={onCoverSubmit} uniqueId="2" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          {/*Article Image */}
          <div>
            <table
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                textAlign: "center"
              }}
            >
              <tbody>
                <tr>
                  <td>
                    <h3>Image</h3>
                  </td>
                  <td>
                    <Upload onUpload={onImageSubmit} uniqueId="1" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div>
            <label htmlFor="submit-button">
              <img src="images/Save.png" alt="Save" />
            </label>
            <button
              id="submit-button"
              type="submit"
              onClick={submitHandler}
              style={{ display: "none" }}
            />
          </div>
        </form>
      </div>
    </div>
  );
}

export default ArticleAddApp;
