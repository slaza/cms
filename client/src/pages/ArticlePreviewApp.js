import React from "react";
import Navigation from "../components/Navigation";

function textWithImage(text, url) {
  if (url && url !== "") {
    return (
      <div>
        <table
          style={{ width: "80%", paddingLeft: "10px", paddingRight: "10px" }}
        >
          <tr>
            <td style={{ width: "60%" }}>
              <div style={{ paddingLeft: "40px" }}>{text}</div>
            </td>
            <td style={{ width: "80%" }}>
              <img
                src={url}
                alt="err cover not found"
                style={{ width: "80%", paddingLeft: "40%" }}
              />
            </td>
          </tr>
        </table>
      </div>
    );
  } else {
    return <div style={{ width: "75%" }}>{text}</div>;
  }
}

export default function ArticlePreviewApp(props) {
  const article = props.location.state.article;
  console.log(article);
  return (
    <div style={{ textAlign: "center" }}>
      <Navigation />
      <div>
        <table style={{ width: "100%" }}>
          <tr>
            <td style={{ width: "20%" }}></td>
            <td style={{ width: "60%", textAlign: "center" }}>
              <h1 style={{ textAlign: "center" }}>{article.title}</h1>
            </td>
            <td style={{ width: "20%", color: "#5f92c2" }}>
              <h3 style={{ textAlign: "right", marginRight: "15%" }}>
                {article.time}
              </h3>
            </td>
          </tr>
        </table>
      </div>
      {article.cover && article.cover !== "" && (
        <div>
          <img
            style={{
              maxWidth: "60%",
              textAlign: "center",
              marginLeft: "20%",
              marginRight: "20%",
              maxHeight: "400px"
            }}
            src={article.cover}
            alt="broken link"
          />
        </div>
      )}
      <div
        style={{
          marginLeft: "20%",
          marginRight: "20%",
          marginTop: "30px",
          marginBottom: "30px"
        }}
      ></div>
      {textWithImage(article.content, article.image)}
    </div>
  );
}
