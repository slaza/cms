import React, { useState } from "react";
import { Link } from "react-router-dom";
import getSearchPromise from "../api/API";
import Article from "../components/Article";
import Navigation from "../components/Navigation";

function ArticleApp(props) {
  const [searchResult, setSearchResult] = useState([]);

  const onSearchChange = function(e) {
    console.log(e.target.value);
    getSearchPromise(e.target.value).then(function(result) {
      setSearchResult(result);
    });
  };

  function onArticleDelete(id) {
    console.log(`onArticleDelete: id: ${id}`);
    setSearchResult(searchResult.filter(item => item.id !== id));
  }

  return (
    <div style={{ textAlign: "center" }}>
      <Navigation />
      <div>
        {/*Search*/}
        <div>
          <label>
            <h3> Search for articles</h3>
          </label>
        </div>
        <div>
          <input
            type="text"
            id="search"
            style={{ height: "30px", width: "30%" }}
            onInput={onSearchChange}
          />
          <Link to={`/add_article`}>
            <img
              src="images/Add.png"
              style={{ height: "37px", position: "relative", top: "13px" }}
              alt="Add"
            />
          </Link>
        </div>
      </div>
      <div>
        {searchResult !== null &&
          searchResult.map(article => (
            <div key={article.id}>
              <Article
                style={{ width: "30%" }}
                article={article}
                onDelete={onArticleDelete}
              />
            </div>
          ))}
      </div>
    </div>
  );
}

export default ArticleApp;
