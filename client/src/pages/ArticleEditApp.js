import React, { useState, useEffect } from "react";
import DateTime from "react-datetime";
import moment from "moment";
import { getCategories, getArticleCategories, editArticle } from "../api/API";
import "../css/calendar.css";
import Navigation from "../components/Navigation";
import Upload from "../components/Upload";

function ArticleEdit(props) {
  const dateTimeFormat = "YYYY-MM-DD hh:mm:ss";

  const [article, setArticle] = useState({ text: "", content: "" });
  const [articleCategories, setArticleCategories] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    console.log("effect");
    setArticle(props.location.state.article);
    getArticleCategories(props.location.state.article.id).then(result => {
      setArticleCategories(result);
      console.log(result);
    });

    getCategories().then(result => {
      setCategories(result);
      console.log(result);
    });
  }, [props.location.state.article]);

  function onContentChange(e) {
    let temp = getArticleCopy();
    temp.content = e.target.value;
    setArticle(temp);
  }

  function onTitleChange(e) {
    let temp = getArticleCopy();
    temp.title = e.target.value;
    setArticle(temp);
  }

  function getArticleCopy() {
    return JSON.parse(JSON.stringify(article));
  }

  function onDateChange(e) {
    let temp = getArticleCopy();
    try {
      temp.date = e.format(dateTimeFormat);
      setArticle(temp);
    } catch (err) {
      console.error(err);
    }
  }

  function onSubmitHandler(e) {
    e.preventDefault();
    editArticle(article, articleCategories)
      .then(result => alert("Update successfull"))
      .catch(e => alert(`Error: ${e}`));
  }

  function onCoverSubmit(url) {
    console.log(url);
    let temp = getArticleCopy();
    temp.cover = url;
    setArticle(temp);
  }

  function onImageSubmit(url) {
    console.log(url);
    let temp = getArticleCopy();
    temp.image = url;
    setArticle(temp);
  }

  function isCategorySelected(articleId) {
    for (let i = 0; i < articleCategories.length; i++) {
      if (articleCategories[i].id === articleId) {
        return true;
      }
    }
    return false;
  }

  function onMultiselectChange(e) {
    let selected = [];
    for (let i = 0; i < e.target.length; i++) {
      const item = e.target[i];
      if (item.selected === true) {
        selected.push({ id: item.value });
      }
    }
    for (let i = 0; i < selected.length; i++) {
      for (let j = 0; j < categories.length; j++) {
        if (selected[i].id === categories[j].id) {
          selected[i].name = categories[j].name;
        }
      }
    }
    setArticleCategories(selected);
    selected.map(item => console.log(item));
  }

  return (
    <div style={{ textAlign: "center" }}>
      <Navigation />
      <form>
        <table
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            textAlign: "center"
          }}
        >
          <tbody>
            <tr>
              <td></td>
              <td>
                <h2>
                  <input
                    style={{
                      textAlign: "center",
                      fontSize: "40px",
                      fontWeight: "bold"
                    }}
                    type="text"
                    defaultValue={article.title}
                    onChange={onTitleChange}
                    name="title"
                  />
                </h2>
              </td>
              <td>
                <label for="asd123">
                  <img
                    src="/images/Save.png"
                    alt="Update Article"
                    style={{ width: "50px" }}
                  />
                </label>
                <button
                  id="asd123"
                  onClick={onSubmitHandler}
                  style={{ display: "none" }}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <Upload onUpload={onCoverSubmit} uniqueId="1" />
        </div>
        <div>
          {article.cover !== null && article.cover !== "" && (
            <div>
              <img
                src={article.cover}
                alt={article.cover}
                style={{ width: "50%", maxHeight: "500px" }}
              />
            </div>
          )}
        </div>
        <div>
          <DateTime
            id="dateTime"
            defaultValue={moment(
              props.location.state.article.time,
              dateTimeFormat
            )}
            input={false}
            onChange={onDateChange}
            locale="en-US"
            timeFormat="HH:mm"
            dateFormat
            closeOnSelect
          />
        </div>
        <div>
          <textarea
            name="content"
            value={article.content}
            style={{ width: "70%", resize: "none", height: "400px" }}
            onChange={onContentChange}
          />
        </div>
        <br />
        <div>
          <select
            multiple={true}
            onChange={onMultiselectChange}
            style={{
              height: "75px",
              width: "40%",
              borderRadius: "2px",
              paddingLeft: "10px"
            }}
          >
            {categories.map(category => (
              <option
                style={{ textAlign: "center" }}
                value={category.id}
                selected={isCategorySelected(category.id)}
              >
                {category.name}
              </option>
            ))}
          </select>
        </div>
        <Upload onUpload={onImageSubmit} uniqueId="2" />
      </form>
      <div>
        {article.image !== null && article.image !== "" && (
          <div>
            <img
              src={article.image}
              style={{ maxWidth: "50%", maxHeight: "500px" }}
              alt="broken link"
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default ArticleEdit;
