import React, { useState, useEffect } from "react";
import { gallerySummary } from "../api/API";
import GalleryItem from "../components/GalleryItem";
import GalleryItemAdd from "../components/GalleryItemAdd";
import Navigation from "../components/Navigation";

export default function GalleryApp(props) {
  const [images, setImages] = useState([]);

  useEffect(() => {
    gallerySummary()
      .then(images => {
        console.log(images);
        setImages(images);
      })
      .catch(e => console.error(e));
  }, []);

  function copy(e) {
    return JSON.parse(JSON.stringify(e));
  }

  function onItemAdd(newItem) {
    const newList = copy(images);
    console.log(newList);
    newList.push(newItem);
    console.log(newList);
    setImages(newList);
  }

  function onDelete(id) {
    console.log(`deleted item with id: ${id}`);
    let temp = copy(images);
    console.log(temp);
    temp = temp.filter(item => item.id !== id);
    console.log(temp);
    setImages(temp);
  }

  return (
    <div style={{ textAlign: "center" }}>
      <Navigation />
      <GalleryItemAdd onNew={onItemAdd} />
      {(images || []).map(image => (
        <GalleryItem
          key={image.id}
          id={image.id}
          description={image.description}
          path={image.path}
          onDelete={onDelete}
        />
      ))}
    </div>
  );
}
