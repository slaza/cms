import React, { useState, useEffect } from "react";
import Timeline from "../components/Timeline";
import Navigation from "../components/Navigation";

function initTimeline(id, title, order_number, events) {
  let t = { id: id, title: title, events: events, order_number: order_number };
  return t;
}

function initEvent(id, tId, date, description) {
  let e = { id: id, timelineId: tId, date: date, description: description };
  return e;
}

function TimelineApp() {
  const [timelines, setTimelines] = useState([]);

  useEffect(() => {
    const url = "http://127.0.0.1:8080/timeline/timeline_summary";
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(function(response) {
        let timelineList = [];
        (response.Timelines || []).forEach(timeline => {
          let events = [];
          (timeline.events || []).forEach(event =>
            events.push(
              initEvent(
                event.id,
                event.timeline_id,
                event.date,
                event.description
              )
            )
          );

          let t = initTimeline(
            timeline.timeline.id,
            timeline.timeline.title,
            timeline.timeline.order_number,
            events
          );
          timelineList.push(t);
        });
        setTimelines(timelineList);
      })
      .catch(err => {
        console.error(`Error on timeline_summary ${err}`);
      });
  }, []);

  return (
    <div>
      <Navigation />
      {timelines.map(t => (
        <ul key={t.id}>
          <Timeline
            key={t.id}
            orderNumber={t.order_number}
            tId={t.id}
            title={t.title}
            events={t.events}
          />
        </ul>
      ))}
    </div>
  );
}

export default TimelineApp;
