import React, { useState } from "react";

/*
 * params: eId, timelineId, date, description
 */
function Event(props) {
  const [isEdited, setIsEdited] = useState(false);
  return (
    <div>
      <h4>{props.description}</h4>
      <h2>{props.date}</h2>
      <h3>Value of isEdited = {isEdited ? "true" : "false"}</h3>
      <button onClick={() => setIsEdited(!isEdited)}>Edit</button>
    </div>
  );
}

export default Event;
