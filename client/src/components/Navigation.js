import React from "react";
import { Link } from "react-router-dom";

function Navigation() {
  return (
    <div
      style={{
        backgroundColor: "#2b6aa6",
        width: "100%",
        fontWeight: "bold"
      }}
    >
      <table>
        <tbody>
          <tr>
            <td>
              <Link to="/article" style={{ textDecoration: "none" }}>
                <h3
                  style={{
                    marginLeft: "15px",
                    marginRight: "10px",
                    marginTop: "11px",
                    marginBottom: "11px",
                    color: "#ffffff",
                    textAlign: "center"
                  }}
                >
                  Article
                </h3>
              </Link>
            </td>
            <td>
              <Link to="/timeline" style={{ textDecoration: "none" }}>
                <h3
                  style={{
                    marginLeft: "15px",
                    marginRight: "10px",
                    marginTop: "11px",
                    marginBottom: "11px",
                    color: "#ffffff",
                    textAlign: "center"
                  }}
                >
                  Timeline
                </h3>
              </Link>
            </td>
            <td>
              <Link to="/gallery" style={{ textDecoration: "none" }}>
                <h3
                  style={{
                    marginLeft: "15px",
                    marginRight: "10px",
                    marginTop: "11px",
                    marginBottom: "11px",
                    color: "#ffffff",
                    textAlign: "center"
                  }}
                >
                  Gallery
                </h3>
              </Link>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Navigation;
