import React, { useState } from "react";
import { addGalleryImage } from "../api/API";
import Upload from "./Upload";

export default function GalleryItemAdd(props) {
  const [imagePath, setImagePath] = useState("");
  const [description, setDescription] = useState("");

  function onDescriptionChange(e) {
    setDescription(e.target.value);
  }

  function submit(e) {
    if (!imagePath || imagePath === "") {
      alert("Image not uploaded");
      return;
    }
    if (!description || description === "") {
      alert("Image has no description");
      return;
    }
    addGalleryImage(description, imagePath)
      .then(id => {
        const newEvent = {
          id: id,
          path: imagePath,
          description: description
        };
        props.onNew(newEvent);
        setDescription("");
        setImagePath("");
      })
      .catch(e => console.error(e));
  }

  function onImageChange(url) {
    setImagePath(url);
  }

  return (
    <div>
      <textarea
        value={description}
        onChange={onDescriptionChange}
        style={{ resize: "none", width: "30%", height: "150px" }}
      />
      <Upload onUpload={onImageChange} uniqueId="1" />
      <button
        onClick={submit}
        style={{
          backgroundColor: "#ffffff",
          borderColor: "#2b6aa6",
          borderWidth: "0px"
        }}
      >
        <h3 style={{ color: "#2b6aa6", margin: "3px" }}>
          Add image to gallery
        </h3>
      </button>
    </div>
  );
}
