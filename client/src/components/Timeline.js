import React, { useState, useEffect } from "react";
import HorizontalTimeline from "react-horizontal-timeline";
import DateTime from "react-datetime";
import { addEvent, deleteEvent, updateEvent, updateTimeline } from "../api/API";
import "../css/calendar.css";

//https://codesandbox.io/s/2v37vw5zpp
//https://stackoverflow.com/questions/50815574/reactjs-horizontal-timeline-component
export default function Timeline(props) {
  const dateFormat = "YYYY-MM-DD";

  const [title, setTitle] = useState("");

  const [index, setIndex] = useState(0);
  const [date, setDate] = useState(0);
  const [description, setDescription] = useState("");
  const [events, setEvents] = useState([]);
  const [currentEventDescription, setCurrentEventDescritpion] = useState("");

  function copy(item) {
    return JSON.parse(JSON.stringify(item));
  }

  useEffect(() => {
    console.log("Effect");
    setTitle(props.title);
    setEvents(props.events.sort(eventSort));

    if (props.events.length !== 0) {
      setCurrentEventDescritpion(props.events[0].description);
    }
  }, [props.events, props.title]);

  function onTitleSave() {
    updateTimeline(props.tId, title, 1)
      .then(e => console.log("Update successful"))
      .catch(e => alert(`Error ${e}`));
  }

  function eventSort(ev1, ev2) {
    console.log(ev1.date);
    console.log(ev2.date);
    const d1 = new Date(ev1.date);
    const d2 = new Date(ev2.date);
    console.log(d1 - d2);
    return d1 - d2;
  }

  function deleteEventHandler(e) {
    const eventId = events[index].id;

    if (window.confirm("Are you sure?") === true) {
      deleteEvent(eventId)
        .then(e => {
          let temp = copy(events);
          temp = temp.filter(event => {
            return event.id !== eventId;
          });
          setEvents(temp.sort(eventSort));
        })
        .catch(e => {
          console.error(e);
          alert(`Error ${e}`);
        });
    }
  }

  function addEventHandler(e) {
    console.log(`description:${description}, date: ${date}`);
    if (!description || description === "") {
      alert("Description is empty!");
      return;
    }
    addEvent(props.tId, date, description)
      .then(eventId => {
        const newEvent = {
          id: eventId,
          description: description,
          date: date,
          timelineId: props.tId
        };
        let temp = JSON.parse(JSON.stringify(events));
        temp.push(newEvent);
        setEvents(temp.sort(eventSort));
        setDescription("");
      })
      .catch(e => alert(`Error ${e}`));
  }

  function updateEventHandler(e) {
    const event = events[index];
    updateEvent(event.id, event.timelineId, currentEventDescription, event.date)
      .then(e => {
        let temp = copy(events);
        temp[index].description = currentEventDescription;
        setEvents(temp);
        alert("Update successful");
      })
      .catch(e => {
        console.error(e);
        alert(`Error ${e}`);
      });
  }

  function onDescriptionChange(e) {
    console.log(`description: ${e.target.value}`);
    setDescription(e.target.value);
  }

  function onDateChange(e) {
    try {
      setDate(e.format(dateFormat));
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div>
      <div style={{ height: "45px" }}>
        <input
          type="text"
          value={title}
          style={{
            fontWeight: "bolder",
            fontSize: "15px",
            width: "20%",
            paddingLeft: "18px"
          }}
          onChange={e => setTitle(e.target.value)}
        />
        <label htmlFor={`titleChange-${props.tId}`}>
          <img
            src="images/Save.png"
            style={{ height: "45px", verticalAlign: "middle" }}
            alt="broken link"
          />
        </label>
        <button
          id={`titleChange-${props.tId}`}
          onClick={onTitleSave}
          style={{ display: "none" }}
        />
      </div>
      <div
        style={{
          width: "60%",
          height: "100px",
          margin: "0 auto",
          marginTop: "20px",
          fontSize: "15px"
        }}
      >
        <HorizontalTimeline
          styles={{
            background: "#f8f8f8",
            foreground: "#1A79AD",
            outline: "#dfdfdf"
          }}
          index={index}
          indexClick={index => {
            setIndex(index);
            setCurrentEventDescritpion(events[index].description);
          }}
          values={events.map(e => e.date)}
        />
      </div>
      <div style={{ textAlign: "center" }}>
        <div>
          <textarea
            id="textarea-current-event"
            style={{
              resize: "none",
              width: "40%",
              height: "100px"
            }}
            value={currentEventDescription}
            onChange={e => setCurrentEventDescritpion(e.target.value)}
          />
        </div>
        <div style={{ height: "45px" }}>
          <label htmlFor={`update-button-${props.tId}`}>
            <img
              src="/images/Save.png"
              style={{ height: "45px" }}
              alt="broken link"
            />
          </label>
          <button
            id={`update-button-${props.tId}`}
            onClick={updateEventHandler}
            style={{ display: "none" }}
          />
          <label htmlFor={`delete-button-${props.tId}`}>
            <img
              alt="broken link"
              src="images/Delete.png"
              style={{ height: "45px" }}
            />
          </label>
          <button
            id={`delete-button-${props.tId}`}
            onClick={deleteEventHandler}
            style={{ display: "none" }}
          />
        </div>
      </div>
      <div style={{ textAlign: "center" }}>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td style={{ width: "30%" }}></td>
              <td style={{ width: "40%" }}>
                <h3
                  style={{
                    fontWeight: "bold",
                    width: "100%",
                    background: "#3366ff",
                    color: "#ffffff",
                    height: "45px",
                    textAlign: "center",
                    paddingTop: "8px",
                    paddingBottom: "0px",
                    marginBottom: "0px"
                  }}
                >
                  New Event
                </h3>
              </td>
              <td style={{ width: "30%" }}></td>
            </tr>
          </tbody>
        </table>
        <div>
          <textarea
            id="textarea-new-event"
            value={description}
            onChange={onDescriptionChange}
            style={{
              resize: "none",
              width: "40%",
              height: "100px"
            }}
          />
          <DateTime
            id="date"
            input={false}
            onChange={onDateChange}
            locale="en-US"
            dateFormat
            closeOnSelect
            timeFormat={false}
          />
          <div>
            <label htmlFor={`submit-button-${props.tId}`}>
              <img
                alt="broken link"
                src="images/Add.png"
                style={{ height: "45px" }}
              />
            </label>
            <button
              id={`submit-button-${props.tId}`}
              onClick={addEventHandler}
              style={{ display: "none" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
