import React, { useState } from "react";
import { uploadImage } from "../api/API";

//props.onUpload(path)
//props.uniqueId (unique key needed when multiple upload components are present)
function Upload(props) {
  const [imgPath, setImgPath] = useState("");

  function handleChange(event) {
    uploadImage(event.target.files[0])
      .then(url => {
        console.log(url);
        setImgPath(url);
        props.onUpload(url);
      })
      .catch(err => console.error(`Error on upload: ${err}`));
  }

  return (
    <div>
      <div style={{ height: "45px" }}>
        <div class="image-upload">
          <label htmlFor={`file-input-${props.uniqueId}`}>
            <img
              src={"/images/Upload.png"}
              style={{ height: "45px" }}
              alt="Upload Image"
            />
          </label>
          <input
            id={`file-input-${props.uniqueId}`}
            type="file"
            onChange={handleChange}
            style={{ display: "none" }}
          />
          {imgPath && imgPath !== "" && (
            <img src={imgPath} style={{ height: "45px" }} alt="broken link" />
          )}
        </div>
      </div>
    </div>
  );
}

export default Upload;
