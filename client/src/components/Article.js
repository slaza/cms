import React from "react";
import { Link } from "react-router-dom";
import { deleteArticle } from "../api/API";

function Article(props) {
  function deleteHandler() {
    if (window.confirm("Are you sure?") === true) {
      deleteArticle(props.article)
        .then(result => {
          console.log("Success");
          props.onDelete(props.article.id);
        })
        .catch(e => {
          alert("Error");
          console.error(e);
        });
    }
  }
  return (
    <div
      style={{
        width: "34%",
        textAlign: "center",
        marginLeft: "33%",
        marginRight: "33%",
        border: "groove",
        borderWidth: "1px",
        borderColor: "#cce6ff",
        borderBottomWidth: "5px",
        borderLeftWidth: "5px",
        borderBottomColor: "#99ccff",
        marginBottom: "10px"
      }}
    >
      <div>
        <Link
          style={{ textDecoration: "none" }}
          to={{
            pathname: `/preview_article`,
            state: {
              article: props.article
            }
          }}
        >
          <div>
            <table>
              <tbody>
                <tr>
                  <td style={{ width: "15%" }}></td>
                  <td style={{ textAlign: "center", width: "70%" }}>
                    <h2
                      style={{
                        color: "#2e6ca6",
                        textAlign: "center"
                      }}
                    >
                      {props.article.title}
                    </h2>
                  </td>
                  <td style={{ width: "15%" }}>
                    <h5 style={{ color: "#07437a", fontSize: "10px" }}>
                      {props.article.time}
                    </h5>
                  </td>
                </tr>
              </tbody>
            </table>
            <div
              style={{
                fontSize: "12px",
                color: "#0b1621",
                textAlign: "left",
                marginLeft: "15%",
                marginRight: "15%"
              }}
            >
              {props.article.content.substring(0, 300)}
            </div>
          </div>
        </Link>
      </div>
      <div style={{ textAlign: "right", height: "45px" }}>
        <Link
          to={{
            pathname: `/edit_article/${props.article.id}`,
            state: {
              article: props.article
            }
          }}
        >
          <img
            src="/images/Edit.png"
            alt="Edit"
            style={{ height: "35px", right: "0px", bottom: "0px" }}
          />
        </Link>
        <img
          src="/images/Delete.png"
          alt="Delete"
          onClick={deleteHandler}
          style={{ height: "35px", right: "0px", bottom: "1px" }}
        />
      </div>
    </div>
  );
}

export default Article;
