import React, { useState } from "react";
import { deleteGalleryImage, updateGalleryImage } from "../api/API";
//onDelete exported
//onUpdate not exported
function GalleryItem(props) {
  const [imageUrl, setImageUrl] = useState(props.path);
  const [description, setDescription] = useState(props.description);

  function updateHandler(e) {
    updateGalleryImage(props.id, description, imageUrl)
      .then(alert("Update successful"))
      .catch(e => console.error(e));
  }

  function deleteHandler(e) {
    if (window.confirm("Are you sure?") === true) {
      deleteGalleryImage(props.id)
        .then(e => {
          props.onDelete(props.id);
        })
        .catch(e => console.log(e));
    }
  }

  return (
    <div>
      <img
        src={imageUrl}
        alt="broken link"
        style={{ height: "200px", maxWidth: "500px" }}
      />
      <textarea
        value={description}
        style={{
          width: "30%",
          height: "200px",
          resize: "none"
        }}
        onChange={e => setDescription(e.target.value)}
      />
      <div>
        <label htmlFor={`update-button-${props.id}`}>
          <img src="images/Save.png" style={{ height: "45px" }} alt="Save" />
        </label>
        <button
          id={`update-button-${props.id}`}
          onClick={updateHandler}
          style={{ display: "none" }}
        />
        <label htmlFor={`delete-button-${props.id}`}>
          <img
            src="images/Delete.png"
            style={{ height: "45px" }}
            alt="Delete"
          />
        </label>
        <button
          id={`delete-button-${props.id}`}
          onClick={deleteHandler}
          style={{ display: "none" }}
        />
      </div>
      <br />
    </div>
  );
}

export default GalleryItem;
